(function($) {
    $( document ).ready(function() {
        var elements = drupalSettings.image_slider.SlidesJS;
        $.each(elements, function( index, value ) {
            var element = $('.' + index);
            if ( element.length > 0 ) {
                var width = value.width;
                var height = value.height;
                var float = value.float;
                if (float instanceof Object) {
                    float = float.value;
                }
                var animation = value.animation;
                element.css({
                    'float': float,
                    'display':'inline-table',
                    'width': width,
                    'height': height,
                    'opacity': '0'
                });
                element.find($('.slides')).slidesjs({
                    callback: {
                        loaded: function(number) {
                            element.find($(".slidesjs-container")).css({
                                'width': 'initial',
                                'height': height
                            });
                            if (!isNaN(parseInt(height))) {
                                element.find($(".slidesjs-control")).css({
                                    'width': 'initial',
                                    'height': height
                                });
                            }
                            element.find($('.slides')).css({
                                'display': 'inline'
                            });
                            element.fadeTo('slow', 1);
                        }
                    },
                    play: {
                        active: parseInt(animation.enabled),
                        auto: parseInt(animation.auto),
                        effect: animation.effect,
                        interval: parseInt(animation.interval),
                        swap: true
                    },
                    navigation: {
                        effect: animation.effect
                    },
                    pagination: {
                        effect: animation.effect
                    },
                    effect: {
                        fade: {
                            speed: typeof(animation.speed) !== 'undefined' ? parseInt(animation.speed) : 300
                        },
                        slide: {
                            speed: typeof(animation.speed) !== 'undefined' ? parseInt(animation.speed) : 300
                        }
                    }
                });
            }
        });
    });
})(jQuery);