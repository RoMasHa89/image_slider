<?php

/**
 * @file
 * Contains \Drupal\image_slider\Plugin\Field\FieldFormatter\ImageSlider.
 */

namespace Drupal\image_slider\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatter;

/**
 * Plugin implementation of the 'image_slider' formatter.
 *
 * @FieldFormatter(
 *   id = "image_slider",
 *   label = @Translation("Image Slider"),
 *   field_types = {
 *     "image"
 *   }
 * )
 */
class ImageSlider extends ImageFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode = NULL) {
    $elements = parent::viewElements($items, $langcode);

    // Get slider config.
    $width = $height = $animation_ops = '';
    $float = 'none';
    if (!empty($this->thirdPartySettings['image_slider']['image_slider_width'])) {
      $width = image_slider_get_css_attribute($this->thirdPartySettings['image_slider']['image_slider_width']);
    }
    if (!empty($this->thirdPartySettings['image_slider']['image_slider_height'])) {
      $height = image_slider_get_css_attribute($this->thirdPartySettings['image_slider']['image_slider_height']);
    }
    if (!empty($this->thirdPartySettings['image_slider']['image_slider_float'])) {
      $float = $this->thirdPartySettings['image_slider']['image_slider_float'];
    }
    if (!empty($this->thirdPartySettings['image_slider']['image_slider_animation'])) {
      $animation_ops = $this->thirdPartySettings['image_slider']['image_slider_animation'];
    }
    // Construct field class-name (id for JS).
    $id = 'field--name-' . $items->getName();
    // Add library and data to elements. Apply custom theme.
    foreach ($elements as $key => &$element) {
      // Apply theme to element.
      $element['#theme'] = 'image_slider_formatter_child';
    }
    // Add library and data.
    $elements['#theme'] = 'image_slider_formatter_wrapper';
    $elements['#attached']['library'][] = 'image_slider/SlidesJS';
    $elements['#attached']['drupalSettings']['image_slider']['SlidesJS'][$id]['width'] = $width;
    $elements['#attached']['drupalSettings']['image_slider']['SlidesJS'][$id]['height'] = $height;
    $elements['#attached']['drupalSettings']['image_slider']['SlidesJS'][$id]['float'] = $float;
    $elements['#attached']['drupalSettings']['image_slider']['SlidesJS'][$id]['animation'] = $animation_ops;
    return $elements;
  }

}
